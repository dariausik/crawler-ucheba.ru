﻿using Crawler.Ucheba.Crawler;

namespace Crawler.Ucheba.Demo
{
  class Program
    {
        static void Main()
        {
            UchebaDataLoader.RunCrawler();
        }
    }
}

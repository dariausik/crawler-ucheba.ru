﻿using HtmlAgilityPack;
using System.Net;

namespace Crawler.Ucheba.Proxy
{
  interface IHtmlWeb
    {
        //  HtmlWeb
        HtmlDocument Load(string url);
        HttpStatusCode StatusCode { get; }

        //  WebClient
        byte[] DownloadData(string address);
    }
}

﻿using HtmlAgilityPack;
using System.Net;

namespace Crawler.Ucheba.Proxy
{
  public class HtmlWebWithoutProxy : IHtmlWeb
    {
        HtmlWeb _web;

        public HtmlWebWithoutProxy()
        {
            _web = new HtmlWeb();
        }

        // метод вызывается для настройки каждого запроса
        bool OnPreRequest(HttpWebRequest request)
        {
            request.AllowAutoRedirect = false;
            return true;
        }

        #region IHtmlWeb
        
        public HttpStatusCode StatusCode => _web.StatusCode;

        public HtmlDocument Load(string url) => _web.Load(url);

        public byte[] DownloadData(string address)
        {
            using (var client = new WebClient())
                return client.DownloadData(address);
        }

        #endregion
    }
}

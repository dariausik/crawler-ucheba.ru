﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace Crawler.Ucheba.Proxy
{
  //  Доп. класс для обхода блокировки обращений к серверу, реализует 2 подхода:
  //  - смена прокси-сервера для каждого запроса, 
  //  - паузы в зависимости от количества запросов или времени с момента последней паузы, т.к заранее неизвестны правила, по которым сервер запрещает обращения

  class HtmlWebWithProxy : IHtmlWeb
    {
        readonly HtmlWeb _web;
        readonly List<ProxySettings> _proxyServers;
        int _currentProxyIndex;
        int _requestsDoneCount;
        int _proxyRequestsCount;
        DateTime _lastTimeoutDateTime;
        object _lock;

        //  настройки
        readonly int RequestsCountToChangeProxy;
        readonly int MaxRequestsBetweenTimeouts;
        readonly TimeSpan MaxIntervalBetweenTimeouts;
        readonly TimeSpan TimeoutInterval;

        public HtmlWebWithProxy(bool useProxy)
        {
            _web = new HtmlWeb()
            {
                UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
                PreRequest = OnPreRequest,            
            };

            if(useProxy)
            {
                _proxyServers = ProxySettingsLoader.LoadFromTxt("Proxies.txt");
                
                RequestsCountToChangeProxy = 100000;
                MaxRequestsBetweenTimeouts = 10;
                MaxIntervalBetweenTimeouts = TimeSpan.FromSeconds(30);
                TimeoutInterval = TimeSpan.FromSeconds(3);
            }
            else
            {
                _proxyServers = new List<ProxySettings> { ProxySettings.Create(null, 0) };

                RequestsCountToChangeProxy = int.MaxValue;
                MaxRequestsBetweenTimeouts = 1;
                MaxIntervalBetweenTimeouts = TimeSpan.FromSeconds(30);
                TimeoutInterval = TimeSpan.FromSeconds(0.5);
            }

            _lock = new object();

            _currentProxyIndex = 0;
            _proxyRequestsCount = 0;
            _requestsDoneCount = 0;
        }

        // метод вызывается для настройки каждого запроса
        bool OnPreRequest(HttpWebRequest request)
        {
            request.AllowAutoRedirect = false;
            request.Timeout = 10000;
            request.KeepAlive = false;
            return true;
        }


        public HttpStatusCode StatusCode { get => _web.StatusCode; }

        // загрузка через прокси с автоматической заменой прокси, если загрузка не удалась
        T LoadAndRotateProxyOnException<T>(Func<ProxySettings, T> loadFunc)
        {
            var isProxyFailed = false;
            while (true)
            {
                var proxy = GetNextProxyWithTimeout(isProxyFailed);
                try
                {
                    return loadFunc(proxy);
                }
                catch (Exception ex)
                {
                    isProxyFailed = true;
                    Console.WriteLine($"[{ex.GetType().Name}] {proxy.Host}:{proxy.Port} {ex.Message}");
                }
            }
        }

        // загрузка документа
        public HtmlDocument Load(string url)
        {
            return LoadAndRotateProxyOnException(
                proxy =>
                {
                    if (!string.IsNullOrWhiteSpace(proxy.Host))
                    {
                        var result = _web.Load(url, proxy.Host, proxy.Port, null, null);
                        if (_web.StatusCode != HttpStatusCode.OK)
                            throw new Exception($"HTTP Status Code = {_web.StatusCode}");

                        return result;
                    }

                    return _web.Load(url);
                }
            );
        }
        
        // загрузка картинок
        public byte[] DownloadData(string address)
        {
            return LoadAndRotateProxyOnException(
                proxy =>
                {
                    using var client = new WebClient();
                    if (!string.IsNullOrWhiteSpace(proxy.Host))
                        client.Proxy = new WebProxy(proxy.Host, proxy.Port);

                    return client.DownloadData(address);
                }
            );
        }

        //  метод нужен для ротации прокси серверов и пауз между запросами
        ProxySettings GetNextProxyWithTimeout(bool forceProxyChange)
        {
            lock (_lock)
            {
                _proxyRequestsCount++;
                _requestsDoneCount++;

                //  не пора ли сделать паузу?
                if (_requestsDoneCount >= MaxRequestsBetweenTimeouts || DateTime.Now - _lastTimeoutDateTime >= MaxIntervalBetweenTimeouts)
                {
                    Thread.Sleep(TimeoutInterval);

                    _requestsDoneCount = 0;
                    _lastTimeoutDateTime = DateTime.Now;
                }

                //  смена прокси сервера
                if (forceProxyChange || _proxyRequestsCount >= RequestsCountToChangeProxy)
                {
                    _currentProxyIndex = (_currentProxyIndex + 1) % _proxyServers.Count;
                    _proxyRequestsCount = 0;
                }

                return _proxyServers[_currentProxyIndex];
            }
        }
    }
}

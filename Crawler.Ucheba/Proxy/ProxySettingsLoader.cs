﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Crawler.Ucheba.Proxy
{
  static class ProxySettingsLoader
    {
        static List<ProxySettings> BaseLoad(string filePath, Func<string, ProxySettings> lineParser)
            => File.ReadAllLines(filePath)
                    .Distinct()
                    .Select(lineParser)
                    .Where(proxy => proxy != null)
                    .ToList();


        public static List<ProxySettings> LoadFromTxt(string filePath)  => BaseLoad(filePath, ParseFromTxt);

        static ProxySettings ParseFromTxt(string line)
        {
            var values = line.Split(':', StringSplitOptions.RemoveEmptyEntries);
            if (!values.Any())
                return null;
            return ProxySettings.Create(values[0], int.Parse(values[1]));
        }

        public static List<ProxySettings> LoadFromCsv(string filePath) => BaseLoad(filePath, ParseFromCsv);

        static ProxySettings ParseFromCsv(string line)
        {
            var values = line.Split(';', StringSplitOptions.RemoveEmptyEntries);
            if (!values.Any())
                return null;
            return ProxySettings.Create(values[0], int.Parse(values[1]));
        }
    }
}

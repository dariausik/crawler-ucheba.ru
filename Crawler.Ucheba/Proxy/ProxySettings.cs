﻿namespace Crawler.Ucheba.Proxy
{
  class ProxySettings
    {
        public string Host { get; private set; }
        public int Port { get; private set; }
        public static ProxySettings Create(string host, int port) => new ProxySettings { Host = host, Port = port };
    }
}

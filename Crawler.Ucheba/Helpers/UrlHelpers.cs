﻿using System;

namespace Crawler.Ucheba.Helpers
{
  class UrlHelpers
    {
        public static string GetBaseUrl(string url) => new Uri(url).GetLeftPart(UriPartial.Authority);
    }
}

﻿using Crawler.Ucheba.Helpers;
using Crawler.Ucheba.Models;
using Crawler.Ucheba.Proxy;
using ScrapySharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Crawler.Ucheba.Crawler
{
  class UchebaCrawler
    {
        readonly string _startUrl;
        readonly IHtmlWeb _web;

        public UchebaCrawler(IHtmlWeb web)
        {
            _startUrl = "https://www.ucheba.ru/for-abiturients/vuz";
            _web = web;
        }

        #region NavigationUniversityPage

        public IEnumerable<University> GetUniversities()
            => new UniversityCrawler(_web).GetUniversities(GetUniversitiesUrls());

        public List<string> GetUniversitiesUrls()
            => GetCitiesUrls()
                    .AsParallel()
                    .SelectMany(GetCityUniversitiesUrls)
                    .ToList();

        List<string> GetCitiesUrls()
        {
            var scheme = new Uri(_startUrl).Scheme;
            var citiesUrls = _web.Load(_startUrl).DocumentNode
                .CssSelect(".location-block-city-list > li > a")
                .Select(node => $"{scheme}:{node.Attributes["href"].Value}")
                .ToList();

            return citiesUrls;
        }

        List<string> GetCityUniversitiesUrls(string cityUrl)
        {
            //  TODO:
            //      некоторые прокси возвращают 302, чтобы редиректить на рекламные сайты
            //      - надо переписать условие последней страницы в списке универов, чтобы работать через прокси

//             throw new NotImplementedException();

            var cityUniversityUrls = new List<string>();
            var baseCityUrl = UrlHelpers.GetBaseUrl(cityUrl);

            var currentPageUrl = cityUrl;
            while (true)
            {
                var document = _web.Load(currentPageUrl);
                if (_web.StatusCode != HttpStatusCode.OK)
                    break;

                var urlsOnCurrentPage = document.DocumentNode
                    .CssSelect(".search-results-title > a.js_webstat")
                    .Select(node => baseCityUrl + node.Attributes["href"].Value);

                cityUniversityUrls.AddRange(urlsOnCurrentPage);

                currentPageUrl = $"{cityUrl}?s={cityUniversityUrls.Count}";
            }

            return cityUniversityUrls;
        }
        #endregion
    }
}

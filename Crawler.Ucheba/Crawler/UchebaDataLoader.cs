﻿using Crawler.Ucheba.Models;
using Crawler.Ucheba.Proxy;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;

namespace Crawler.Ucheba.Crawler
{
  public static class UchebaDataLoader
    {
        static readonly string UchebaRuDirectory = "UchebaRuUniversities";
        static readonly string UniversityUrlsFile = "_urls.json";

        public static List<University> LoadUniversitiesFromJsonFiles()
            => Directory.GetFiles(UchebaRuDirectory)
                .Where(filepath => Path.GetFileName(filepath) != UniversityUrlsFile)
                .Select(filepath => JsonSerializer.Deserialize<University>(File.ReadAllText(filepath)))
                .ToList();

        static HashSet<string> GetSavedUniversityUrls()
            => Directory.GetFiles(UchebaRuDirectory)
                .Where(filepath => Path.GetFileName(filepath) != UniversityUrlsFile)
                .Select(s => $"https://{Path.GetFileNameWithoutExtension(s).Replace('_', '/')}")
                .ToHashSet();

        public static void RunCrawler()
        {
            var sw = new Stopwatch();
            sw.Start();

            Directory.CreateDirectory(UchebaRuDirectory);

            // запуск краулера без бана и без прокси
            var (proxy, universitiesUrls) = CrawlerWithoutProxy();

            // запуск краулерa с прокси
            //var (proxy, universitiesUrls) = CrawlerWithProxy();

            //  убираем из списка те университеты, которые уже были загружены
            var listSavedUniversityUrls = GetSavedUniversityUrls();
            universitiesUrls = universitiesUrls.Where(uniUrl => !listSavedUniversityUrls.Contains(uniUrl)).ToList();

            // удаляем дубли ссылок на разных поддоменах
            universitiesUrls = SelectUniqueUrls(universitiesUrls);

            //  получаем и сохраняем вузы
            var crawlerUniversities = new UniversityCrawler(proxy);
            var universities = crawlerUniversities.GetUniversities(universitiesUrls);

            //  каждый универ получаем отдельно, не дожидаясь загрузки всего списка (IEnumerable)
            //  и сохраняем каждый универ в свой файл, чтобы дальше можно было продолжить загрузку с этого места в случае непредвиденного завершения программы
            foreach (var university in universities)
            {
                //  в качестве имени файла будет [url].json
                var uri = new Uri(university.UchebaRuUrl);
                var filename = (uri.Host + uri.PathAndQuery).Replace('/', '_');
                var filePath = @$"{UchebaRuDirectory}\{filename}.json";

                using (var fs = File.Create(filePath))
                using (var jsonWriter = new Utf8JsonWriter(fs, new JsonWriterOptions { Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping }))
                    JsonSerializer.Serialize(jsonWriter, university);

                Console.WriteLine($"Elapsed time: {sw.Elapsed}, [{filePath}] saved");
            }

            sw.Stop();
            Console.WriteLine($"Total elapsed time: {sw.Elapsed}");
        }


        static List<string> SelectUniqueUrls(List<string> universitiesUrls)
        {
            var dic = new Dictionary<string, string>(); 
            foreach(var universityUrl in universitiesUrls)
            {
                var key = new Uri(universityUrl).AbsolutePath;
                if (!dic.ContainsKey(key))
                    dic.Add(key, universityUrl);
            }
            return dic.Values.ToList();
        }
        


        static (HtmlWebWithProxy proxy, List<string> universitiesUrls) CrawlerWithProxy()
            =>
            (
                new HtmlWebWithProxy(true),
                JsonSerializer.Deserialize<List<string>>(File.ReadAllText($@"{UchebaRuDirectory}\{UniversityUrlsFile}"))
            );

        static (HtmlWebWithProxy proxy, List<string> universitiesUrls) CrawlerWithoutProxy()
        {
            var proxy = new HtmlWebWithProxy(false);    //  false - работать без прокси

            //  получаем список ссылок на все вузы
            var crawlerUcheba = new UchebaCrawler(proxy);
            var universitiesUrls = crawlerUcheba.GetUniversitiesUrls();
            using (var fs = File.Create($@"{UchebaRuDirectory}\{UniversityUrlsFile}"))
            using (var jsonWriter = new Utf8JsonWriter(fs, new JsonWriterOptions { Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping }))
                JsonSerializer.Serialize(jsonWriter, universitiesUrls);

            return (proxy, universitiesUrls);
        }


        //  метод для отладки отдельных методов краулера
        static void Test()
        {
            //             var proxy = new HtmlWebWithProxy(true);
            //             var universitiesUrls = JsonSerializer.Deserialize<List<string>>(File.ReadAllText($@"{dir}\_urls.json"));

            var proxy = new HtmlWebWithProxy(true);
            var crawlerUniversity = new UniversityCrawler(proxy);

            // тестовые ссылки на университеты ИТМО, Политех, СПбГУ
            var universities = crawlerUniversity.GetUniversities(new string[]
            {
                "https://spb.ucheba.ru/uz/6213",
                "https://spb.ucheba.ru/uz/10564",
                "https://spb.ucheba.ru/uz/6094",
                "https://www.ucheba.ru/uz/5705",
                "https://spb.ucheba.ru/uz/6142",
            });

            using (var fs = File.Create("Ucheba.json"))
            using (var jsonWriter = new Utf8JsonWriter(fs, new JsonWriterOptions { Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping }))
                JsonSerializer.Serialize(jsonWriter, universities);
        }
    }
}

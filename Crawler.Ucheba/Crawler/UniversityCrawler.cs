﻿using Crawler.Ucheba.Extensions;
using Crawler.Ucheba.Helpers;
using Crawler.Ucheba.Models;
using Crawler.Ucheba.Proxy;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Crawler.Ucheba.Crawler
{
  class UniversityCrawler
    {
        readonly IHtmlWeb _web;

        public UniversityCrawler(IHtmlWeb web)
        {
            _web = web;
        }

        public IEnumerable<University> GetUniversities(IEnumerable<string> universityUrls)
            => universityUrls                    
                    .Select(GetUniversity); //.AsParallel() не делаем, чтобы ускорить загрузку 1 универа - распараллеливать только специальности внутри универа

        public University GetUniversity(string universityUrl)
        {
            var mainDocument = _web.Load(universityUrl).DocumentNode;
            var contactsDocument = _web.Load($"{universityUrl}/contacts").DocumentNode;

            var result = new University
            {
                UchebaRuUrl = universityUrl,
                Icon = GetUniversityIcon(mainDocument),
                Name = GetUniversityName(mainDocument),
                City = GetUniversityCity(mainDocument),
                Description = GetUniversityDescription(mainDocument),
                IsState = GetUniversityIsState(mainDocument),
                HasDormitory = GetUniversityHasDormitory(mainDocument),
                HasMilitaryDepartment = GetUniversityHasMilitary(mainDocument),
                WebPageUrl = GetUniversityWebPageUrl(contactsDocument),
                Email = GetUniversityEmail(contactsDocument),
                Address = GetUniversityAdress(contactsDocument),
                PhoneNumber = GetUniversityPhoneNumber(contactsDocument),
                SocialNets = GetUniversitySocialNets(contactsDocument),
                Specialities = GetSpecialities(universityUrl)
            };
            return result;
        }

        #region MainPage

        byte[] GetUniversityIcon(HtmlNode document)
        {
            var iconUrl = document.CssSelect("img.head-announce__img").First().Attributes["src"].Value;
            if (iconUrl == "/img/no_logo.png")
                return null;
            return _web.DownloadData(iconUrl);
        }
        string GetUniversityName(HtmlNode document) => document.CssSelect(".head-announce__title").First().InnerText;
        string GetUniversityCity(HtmlNode document) => document.CssSelect("ul.params-list > li").First().CssSelect("span").Last().InnerText.Trim();
        string GetUniversityDescription(HtmlNode document) => document.CssSelect("section.mb-section-large > p").FirstOrDefault()?.GetDecodedText();
        bool GetUniversityIsState(HtmlNode document) => document.CssSelect(".params-list > li > span").Any(a => a.InnerText.Trim() == "Государственный вуз");
        bool GetUniversityHasDormitory(HtmlNode document) => document.CssSelect(".params-list > li > span").Any(a => a.InnerText.Trim() == "Есть общежитие");
        bool GetUniversityHasMilitary(HtmlNode document) => document.CssSelect(".params-list > li > span").Any(a => a.InnerText.Trim().StartsWith("Есть военный"));

        #endregion

        #region Contacts

        string GetUniversityWebPageUrl(HtmlNode document)
            => document.SelectSingleNode("(//span[contains(@class, 'address-panel-title')])[1]/parent::li").InnerText.CleanFromHtml();
        string GetUniversityEmail(HtmlNode document)
            => document.SelectSingleNode("(//span[contains(@class, 'address-panel-title')])[2]/parent::li").InnerText.CleanFromHtml();
        string GetUniversityPhoneNumber(HtmlNode document) => document.CssSelect(".address-panel").First().CssSelect("ul").Last().ChildNodes[1].InnerText.Trim();
        string GetUniversityAdress(HtmlNode document) => document.CssSelect(".address-panel").Last().CssSelect("li").First().InnerText.Trim();
        List<string> GetUniversitySocialNets(HtmlNode document) => document.CssSelect("a.icons-link").Select(node => node.Attributes["href"].Value).ToList();

        #endregion

        #region NavigationSpecialities

        List<Speciality> GetSpecialities(string universityUrl)
            => GetSpecialitiesUrls(universityUrl)
                 .AsParallel()
                 .Select(new SpecialityCrawler(_web).GetSpeciality)
                 .Where(speciality => speciality != null)
                 .ToList();

        List<string> GetSpecialitiesUrls(string universityUrl)
        {
            var specialitiesUrls = new List<string>();
            var baseCityUrl = UrlHelpers.GetBaseUrl(universityUrl);
            var specialitiesUrl = $"{universityUrl}/programs?_lt=r";

            var currentPageUrl = specialitiesUrl;
            while (true)
            {
                var document = _web.Load(currentPageUrl).DocumentNode;

                var specialitiesUrlsOnCurrentPage = document
                    .CssSelect(".search-results-info-item a.js_webstat")
                    .Select(node => baseCityUrl + node.Attributes["href"].Value);

                specialitiesUrls.AddRange(specialitiesUrlsOnCurrentPage);

                if (specialitiesUrls.Count >= GetSpecialitiesCount(document))
                    break;

                currentPageUrl = $"{specialitiesUrl}&s={specialitiesUrls.Count}";
            }

            return specialitiesUrls;
        }

        int GetSpecialitiesCount(HtmlNode document)
        {
            var totalCountText = document.CssSelect(".search-sorting-title").First().CssSelect("span").First().NextSibling.InnerText.Trim();
            return int.Parse(totalCountText.Split(' ')[0]);
        }

        #endregion
    }
}

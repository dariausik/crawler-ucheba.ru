﻿using Crawler.Ucheba.Extensions;
using Crawler.Ucheba.Models;
using Crawler.Ucheba.Proxy;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crawler.Ucheba.Crawler
{
    class SpecialityCrawler
    {
        readonly IHtmlWeb _web;
        static readonly HashSet<string> s_allowedDegrees = new HashSet<string> { "специалитет", "бакалавриат", "магистратура", "аспирантура" };

        public SpecialityCrawler(IHtmlWeb web)
        {
            _web = web;
        }

        public Speciality GetSpeciality(string specialityUrl)
        {
            var document = _web.Load(specialityUrl).DocumentNode;

            //  курсы игнорируем, т.к. у них другая структура StudyCondition
            var degree = GetSpecialityDegree(document);
            if (!s_allowedDegrees.Contains(degree.ToLower()))
                return null;

            var result = new Speciality
            {
                Name = GetSpecialityName(document),
                Degree = degree,
                Description = GetSpecialityDescription(document),
                SchoolSubjects = GetSpecialitySchoolSubjects(document),
                AdditionalPassingScore = GetSpecialityAdditionalPassingScore(document),
                Profiles = GetSpecialityProfiles(document),
                StudyConditions = GetStudyConditions(document)
            };
            CopySpecialityPropertiesToStudyConditions(result.StudyConditions, result.Degree, result.SchoolSubjects);

            return result;
        }
 
        string GetSpecialityName(HtmlNode document) => document.SelectSingleNode("//div[contains(@class, 'head-announce__col-right')]/h1").InnerText;

        string GetSpecialityDegree(HtmlNode document) => document.SelectSingleNode("//div[contains(@class, 'head-announce__col-right')]/ul/li").GetTrimmedText();

        #region Speciality parameters from <section> nodes

        HtmlNodeCollection GetSpecialitySectionNodes(HtmlNode document) => document.SelectNodes("//article//section");

        string GetSectionNodeHeader(HtmlNode section)
            => section.SelectSingleNode("div/h2")?.GetDirectInnerText().HtmlDecodeEx().RemoveWhiteSpaces();


        string GetSpecialityDescription(HtmlNode document)
        {
            var section = GetSpecialitySectionNodes(document).FirstOrDefault(section => GetSectionNodeHeader(section) == "О программе");
            if (section == null)
                return null;
            return section.SelectSingleNode("p").GetDecodedText();
        }

        List<string> GetSpecialitySchoolSubjects(HtmlNode document)
        {
            var section = GetSpecialitySectionNodes(document).FirstOrDefault(section => GetSectionNodeHeader(section) == "Условия поступления");
            if (section == null)
                return null;

            return section.SelectNodes("div[2]/div/ul[1]/li")
                          ?.Select(node => node.GetTrimmedText()).ToList(); ;
        }
        #endregion

        List<string> GetSpecialityAdditionalPassingScore(HtmlNode document)
        {
            var section = GetSpecialitySectionNodes(document).FirstOrDefault(section => GetSectionNodeHeader(section) == "Дополнительные баллы к сумме ЕГЭ");
            if (section == null)
                return null;
            return section.SelectNodes(@"ul/li/span").Select(span => span.GetDirectInnerText().RemoveWhiteSpaces()).ToList();
        }

        List<string> GetSpecialityProfiles(HtmlNode document)
        {
            var section = GetSpecialitySectionNodes(document).FirstOrDefault(section => GetSectionNodeHeader(section) == "Профили обучения");
            if (section == null)
                return null;
            return section.SelectSingleNode("p").GetDirectInnerText().Split(';', StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToList();
        }

        #region StudyConditions

        List<StudyCondition> GetStudyConditions(HtmlNode document)
            => document.SelectNodes("//div[contains(@class, 'program-table__row') and position()>1]")
                .Select(GetStudyCondition)
                .ToList();

        public StudyCondition GetStudyCondition(HtmlNode studyRowNode)
        {
            var result = new StudyCondition
            {
                StudyForm = GetStudyConditionStudyForm(studyRowNode),

                TotalBudgetPassingScore = GetStudyConditionTotalBudgetPassMark(studyRowNode),
                TotalCommercialPassingScore = GetStudyConditionTotalCommercialPassMark(studyRowNode),

                BudgetCapacity = GetStudyConditionBudgetCapacity(studyRowNode),
                CommercialCapacity = GetStudyConditionCommercialCapacity(studyRowNode),
                Price = GetStudyConditionPrice(studyRowNode),
                TrainingPeriodInYears = GetStudyConditionTrainingPeriodInYears(studyRowNode),
            };
            return result;
        }

        string GetStudyConditionStudyForm(HtmlNode studyRowNode)
            => studyRowNode.SelectSingleNode(@"div[1]/div").GetTrimmedText();

        float? GetStudyConditionTotalBudgetPassMark(HtmlNode studyRowNode) 
            => float.TryParse(studyRowNode.SelectSingleNode("div[2]").GetTrimmedText(),
                    out var budgetPassMark)
                ? budgetPassMark
                : (float?)null;

        float? GetStudyConditionTotalCommercialPassMark(HtmlNode studyRowNode) 
            => float.TryParse(studyRowNode.SelectSingleNode("div[2]/span/span").GetTrimmedText(),
                    out var commercialPassMark)
                ? commercialPassMark
                : (float?)null;

        int? GetStudyConditionBudgetCapacity(HtmlNode studyRowNode) 
            => int.TryParse(
                    studyRowNode.SelectSingleNode("div[4]").GetTrimmedText(),
                    out var budgetCapacity)
                ? budgetCapacity
                : (int?)null;

        int? GetStudyConditionCommercialCapacity(HtmlNode studyRowNode)
          => int.TryParse(
                  studyRowNode.SelectSingleNode("div[4]/span/span").GetTrimmedText(),
                  out var commercialCapacity)
              ? commercialCapacity
              : (int?)null;


        int? GetStudyConditionPrice(HtmlNode studyRowNode)
            => int.TryParse(studyRowNode.SelectSingleNode("div[6]/div/span")?.GetDecodedText().Replace("р.", "").Replace(" ", ""),
                    out var price)
                ? price
                : (int?)null;


        float? GetStudyConditionTrainingPeriodInYears(HtmlNode studyRowNode)
            => ParseTrainingPeriodInYears(studyRowNode.SelectSingleNode("div[8]").GetTrimmedText());

        float? ParseTrainingPeriodInYears(string periodValue)
        {
            var period = periodValue.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (period.Length < 2)
                return null;

            return period[1].StartsWith("месяц") 
                ? float.Parse(period[0]) / 12 
                : float.Parse(period[0]);
        }
        #endregion

        void CopySpecialityPropertiesToStudyConditions(List<StudyCondition> studyConditions, string degree, List<string> schoolSubjects)
        {
            foreach(var studyCondition in studyConditions)
            {
                studyCondition.Degree_CopyFromSpeciality = degree;
                studyCondition.SchoolSubjects_CopyFromSpeciality = schoolSubjects;
            }
        }
    }
}


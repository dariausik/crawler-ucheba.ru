﻿using System.Collections.Generic;

namespace Crawler.Ucheba.Models
{
  public class StudyCondition
    {
        public string StudyForm { get; set; } // очно, заочно и тд

        public float? TotalBudgetPassingScore { get; set; } // общий проходой бал за 3 экзамена
        public float? TotalCommercialPassingScore { get; set; } // общий проходой бал за 3 экзамена

        public int? BudgetCapacity { get; set; } // мест бюджет
        public int? CommercialCapacity { get; set; } // мест платно

        public int? Price { get; set; } // стоимость
        
        public float? TrainingPeriodInYears { get; set; } // срок обучения


        //  дублируем поля из специальности
        public List<string> SchoolSubjects_CopyFromSpeciality { get; set; }
        public string Degree_CopyFromSpeciality { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace Crawler.Ucheba.Models
{
  public class Speciality
    {
        public string Name { get; set; }
        public string Degree { get; set; } // бакалавриат, специалитет и тд     
        public string Description { get; set; }
        public List<string> SchoolSubjects { get; set; } //предметы егэ для поступления 
        public List<string> AdditionalPassingScore { get; set; } // дополнительные балы к егэ
        public List<string> Profiles { get; set; } //  профили обучения (фильтры для поиска?)
        public List<StudyCondition> StudyConditions { get; set; }
    }
}

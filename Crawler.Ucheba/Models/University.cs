﻿using System.Collections.Generic;

namespace Crawler.Ucheba.Models
{
  public class University
    {
        public string UchebaRuUrl { get; set; }

        public byte[] Icon { get; set; }
//         public string Acronym { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public string WebPageUrl { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public List<string> SocialNets { get; set; }
        public bool IsState { get; set; }
        public bool HasMilitaryDepartment { get; set; }
        public bool HasDormitory { get; set; }
        public List<Speciality> Specialities { get; set; }
    }
}
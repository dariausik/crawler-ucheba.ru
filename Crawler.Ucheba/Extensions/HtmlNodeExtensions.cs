﻿using HtmlAgilityPack;

namespace Crawler.Ucheba.Extensions
{
  static class HtmlNodeExtensions
    {
        public static string GetTrimmedText(this HtmlNode htmlNode) => htmlNode?.GetDirectInnerText().Trim();
        
        public static string GetDecodedText(this HtmlNode htmlNode) => htmlNode?.GetDirectInnerText().Trim().HtmlDecodeEx();
        
        public static string GetCleanedText(this HtmlNode htmlNode) => htmlNode?.GetDirectInnerText().CleanFromHtml();
    }
}

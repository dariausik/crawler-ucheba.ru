﻿using System;
using System.Web;

namespace Crawler.Ucheba.Extensions
{
  static class HtmlStringExtensions
    {
        public static string HtmlDecodeEx(this string htmlString) => HttpUtility.HtmlDecode(htmlString).Replace('\x0a0', ' ');

        public static string RemoveWhiteSpaces(this string s) => (s == null) ? null : string.Join(' ', s.Split("\n\t ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

        public static string CleanFromHtml(this string s) => s.HtmlDecodeEx().RemoveWhiteSpaces();
    }
}
